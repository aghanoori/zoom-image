(function(window) {
    let defineLibrary = () =>( {
        init:function(gallaryid){
            let container = document.querySelector(gallaryid);

            if(! container) {
                console.error('container not exsist.')
                return;
            };
            let
             firstImg = container.querySelector('.small-preview');
            let zoomImg = container.querySelector('.zoomed-image');

            if(! firstImg) {
                console.error('firstImg not exsist.')
                return;
            };

            if(! zoomImg) {
                console.error('zoomImg not exsist.')
                return;
            };

            zoomImg.style.backgroundImage = `url(${firstImg.src})`;

            container.addEventListener('click' ,function(e) {
                let elm = e.target;
                if(elm.classList.contains('small-preview')){
                    zoomImg.style.backgroundImage =`url(${elm.src})`;
                };
                
            });

            zoomImg.addEventListener('mouseenter' , function(){
                this.style.backgroundSize ="250%";
            });

            zoomImg.addEventListener('mousemove' , function(e){
                let dimentions = this.getBoundingClientRect();

                //gharar dadan tasvir dar noghteye 0 , 0 ke goosheye bala samtechap mishavad;
                let x = e.clientX - dimentions.left;
                let y = e.clientY - dimentions.top;
                
                //tabdil kardan x , y be darsad baraye estefade dar backgroundPosition=% ,%
                x = Math.round((100/(dimentions.width/x)));
                y = Math.round((100/(dimentions.height/y)));
                
                this.style.backgroundPosition = `${x}% ${y}%`;
            });

            zoomImg.addEventListener('mouseleave' , function(){
                this.style.backgroundSize ="cover";
                this.style.backgroundPosition ="center";
            });
        }
    });

    if(typeof(vanillaZoom) === "undefined") {
        window.vanillaZoom = defineLibrary();
    }else{
        console.error('vanillaZoom ollredy exsist.')
    };

})(window);